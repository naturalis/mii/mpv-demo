lastkeypressed = ''
playlists = {}

function scanforplaylists() {
    // scans the current dictory for playlists with the file format key-x.txt
    curpath = mp.utils.getcwd()

    files = mp.utils.readdir(curpath, 'files')
    files.forEach(function(filename, index,array) {
        var re = /key-(.)\.txt/gi;
        found = re.exec(filename);
        if (found) {
            playlists[found[1]] = found[0];
        }
    });
}

function setkeys() {
    // set the keys and their corresponding playlists
    for (var key in playlists) {
        mp.add_forced_key_binding(key, "play_" + key, play)
    }
}

function killkeys() {
    // remove the key bindings
    for (var key in playlists) {
        mp.remove_key_binding("play_" + key)
    }
}

function play() {
    // start playing the playlist set to the key
    killkeys()

    //mp.msg.info(lastkeypressed)
    cwd = mp.utils.getcwd()
    mp.commandv('loadlist', cwd + '/' + playlists[lastkeypressed])
    mp.set_property('loop-playlist', 'no')
}

function restart_loop() {
    setkeys()

    //mp.msg.info('restart playlist')
    cwd = mp.utils.getcwd()
    mp.commandv('loadlist', cwd + '/loop.txt')
    mp.set_property('loop-playlist', 'inf')
}

function mp_event_loop() {
    // the event loop waits for a keypress and registers that key
    var wait = 0;
    do {
        var e = mp.wait_event(wait);
        if (e.event != "none") {
            if (e.event == "client-message") {
                if (e.args[0] == 'key-binding') {
                    lastkeypressed = e.args[e.args.length - 1]
                }
            }
            mp.dispatch_event(e);
            wait = 0;
        } else {
            wait = mp.process_timers() / 1000;
            if (wait != 0) {
                mp.notify_idle_observers();
                wait = mp.peek_timers_wait() / 1000;
            }
        }
    } while (mp.keep_running);
}

scanforplaylists()
mp.register_event("idle", restart_loop)
