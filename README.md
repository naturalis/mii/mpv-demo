# mpv-demo
MPV voorbeeld installatie voor Naturalis museum

## Installatie

* Installeer de meest recente versie van [mpv](https://mpv.io).
* Clone deze repository
* `cd` in de directory
* Start met het commando `mpv --config-dir=config`

De standaard configuratie is fullscreen. Indien niet gewenst, zet `fullscreen=no` in `config/mpv.conf`.

## Werking

De config file `config/mpv.conf` bevat een verwijzing naar `scripts/back-to-loop.js`.

Dit script leest de lokale directory uit op zoek naar bestanden met de naam `key-x.txt`, waar de x staat
voor de letter van de toets die geconfigureerd wordt. Op het moment dat mpv merkt dat het niets
afspeelt worden de toetsen van de playlists met `key` gezet en start de `loop.txt` playlist.

Als op een van de voorgedefinieerde knoppen wordt gedrukt begint de corresponderende playlist te spelen. Hierna
worden alle knoppen uitgezet en is het filmpje niet meer te onderbreken. Op het moment dat het filmpje
klaar is met spelen begint de loop weer en zijn de knoppen weer gezet.

Complexere configuraties of andere varianten zijn vanzelfsprekend ook mogelijk. MPV is flexibel.
